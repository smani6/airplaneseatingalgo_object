import numpy
from LeftColum import LeftColumn
from RightColumn import RightColumn
from MiddleColumn import MiddleColumn
import itertools


class SeatAllocationAlgorithm(object):

    def __init__(self, inp_list, max_seat):
        self.inp_list = inp_list
        self.max_seat = max_seat
        self.last_seat = 0
        self.max_x = 0
        self.col_count = 0
        self.window_seats = []
        self.middle_seats = []
        self.asile_seats = []

    def allocate_seats(self):
        """
            Method to allocate asile, window and center seats
            in the seating order.
            Will process each_row based on the inner input array
            length (till each input column) and assign seats to it
            based on certain conditions.

        """
        try:
            self.max_x, self.col_count = self.__get_max_row_count(self.inp_list)

            for inp in range(0, len(self.inp_list)):
                if inp == 0:
                    # Left Column - instantiating LeftColumn Class and calling respective
                    # methods to return the req values
                    left_coln_obj = LeftColumn(self.inp_list[inp])
                    self.window_seats.append(left_coln_obj.window_seats())
                    self.asile_seats.append(left_coln_obj.asile_seats())
                    self.middle_seats.append(left_coln_obj.middle_seats())

                elif inp == (len(self.inp_list) - 1):
                    # Right Column - instantiating RightColumn Class and calling respective
                    # methods to return the req values
                    prev_col_val = self.get_prev_col_val(inp)
                    right_coln_obj = RightColumn(self.inp_list[inp])

                    window = self.inc_prev_col_val(right_coln_obj.window_seats(), prev_col_val)
                    self.window_seats.append(window)

                    asile = self.inc_prev_col_val(right_coln_obj.asile_seats(), prev_col_val)
                    self.asile_seats.append(asile)

                    middle = self.inc_prev_col_val(right_coln_obj.middle_seats(), prev_col_val)
                    self.middle_seats.append(middle)

                else:
                    # Middle Column - instantiating MiddleColumn Class and calling respective
                    # methods to return the req values
                    prev_col_val = self.get_prev_col_val(inp)
                    middle_coln_obj = MiddleColumn(self.inp_list[inp])
                    asile = self.inc_prev_col_val(middle_coln_obj.asile_seats(), prev_col_val)
                    self.asile_seats.append(asile)

                    middle = self.inc_prev_col_val(middle_coln_obj.middle_seats(), prev_col_val)
                    self.middle_seats.append(middle)

            self.asile_seats = self.sorted_res(
                list(
                    itertools.chain.from_iterable(
                        self.asile_seats)))
            self.window_seats = self.sorted_res(itertools.chain.from_iterable(self.window_seats))
            self.middle_seats = self.sorted_res(itertools.chain.from_iterable(self.middle_seats))

            self.print_res()
        except Exception as e:
            print e

    def sorted_res(self, lis):

        return sorted(lis, key=lambda cord: cord[0])

    def get_prev_col_val(self, inp):

        total_col = 0
        for i in range(0, inp):
            array = self.inp_list[i]
            total_col += array[1]

        return total_col

    def inc_prev_col_val(self, res, prev_col):

        new_res = []
        for ele in res:
            lis_ele = list(ele)
            lis_ele[1] = lis_ele[1] + prev_col
            new_res.append(tuple(lis_ele))

        return new_res

    def print_res(self,):

        arrary = numpy.zeros((self.max_x, self.col_count))

        seat_num = 0

        for ele in self.asile_seats:
            if self.max_seat == seat_num:
                break
            seat_num += 1
            arrary[ele[0]][ele[1]] = seat_num

        for ele in self.window_seats:
            if self.max_seat == seat_num:
                break
            seat_num += 1
            arrary[ele[0]][ele[1]] = seat_num

        for ele in self.middle_seats:
            if self.max_seat == seat_num:
                break
            seat_num += 1
            arrary[ele[0]][ele[1]] = seat_num

        seat_matrix = numpy.asmatrix(arrary)
        for x in range(0, self.max_x):
            print "\n"
            row_print = ""
            for y in range(0, self.col_count):
                space_num = 0
                for inp_elt in self.inp_list:
                    space_num += inp_elt[1]
                    if space_num == y:
                        row_print += "\t"
                row_print += "\t"
                if seat_matrix[x, y] != 0:
                    row_print += str(int(seat_matrix[x, y]))
            print row_print

    def __get_max_row_count(self, inp_list):
        """
            Method to return max_row count and
            sum of col_val
        """
        row_list = [inp[0] for inp in inp_list]
        col_list = [inp[1] for inp in inp_list]
        return max(row_list), sum(col_list)

if __name__ == "__main__":

    inp = [[2, 4], [3, 5]]
    max_seat = 30
    mgr_obj = SeatAllocationAlgorithm(inp, max_seat)
    seat_map = mgr_obj.allocate_seats()
