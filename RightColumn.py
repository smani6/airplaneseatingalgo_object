from Column import Column


class RightColumn(Column):

    def __init__(self, value):
        self.array = value
        self.left_array = []
        super(RightColumn, self).create_inp_array()

    def window_seats(self):

        window_seats = []
        for i in range(0, self.array[0]):
            window_seats.append((i, self.array[1] - 1))

        return window_seats

    def asile_seats(self):

        asile_seats = []
        for i in range(0, self.array[0]):
            asile_seats.append((i, 0))
        return asile_seats

    def middle_seats(self):

        middle_seats = []

        for i in range(0, self.array[0]):
            for j in range(0, self.array[1]):
                if j != 0 and j != (self.array[1] - 1):
                    middle_seats.append((i, j))

        return middle_seats
