from Column import Column


class MiddleColumn(Column):

    def __init__(self, value):
        self.array = value
        self.left_array = []
        super(MiddleColumn, self).create_inp_array()

    def window_seats(self):

        return "No Window Seats"

    def asile_seats(self):

        asile_seats = []
        for row in range(0, self.array[0]):
            for col in range(0, self.array[1]):
                if col == 0 or col == self.array[1] - 1:
                    asile_seats.append((row, col))

        return asile_seats

    def middle_seats(self):

        middle_seats = []

        for row in range(0, self.array[0]):
            for col in range(0, self.array[1]):
                if col != 0 and col != self.array[1] - 1:
                    middle_seats.append((row, col))
        return middle_seats
